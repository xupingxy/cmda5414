#define iint int
#define dfloat float

typedef struct {

  iint Nverts, Nfaces;

  iint Nnodes;
  dfloat *VX;
  dfloat *VY;

  iint Nelements;
  iint *EToV;
  iint *EToE;
  iint *EToF;
}mesh2D;

mesh2D* meshReader2D(char *fileName);

void meshPrint2D(mesh2D *mesh);
void meshConnect2D(mesh2D *mesh);

#define mymax(a,b) ((a>b)?a:b)
#define mymin(a,b) ((a<b)?a:b)
