#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mesh2D.h"

//#define iint int
//#define dfloat float

mesh2D* meshReader2D(char* fileName)
{
  /*void meshReader2D(char *fileName,
		  int *Nnodes_out,
		  dfloat **VX,
		  dfloat **VY,
		  int *Ntriangles_out,
		  iint **EToV){
  */
  FILE *fp=fopen(fileName,"r");
  int n;
  mesh2D *mesh = (mesh2D*) calloc(1, sizeof(mesh2D));
  //  int Nnodes, n, Nelements, Nverts;
  //  Nverts = 3;

  mesh->Nverts = 3;
  mesh->Nfaces = 3;
  
  if(fp==NULL)
    {
      printf("meshReader2D: could not load file %s\n", fileName);
      exit(0);
    }

  char buf[BUFSIZ];
  do{
    fgets(buf,BUFSIZ,fp);
  }while(!strstr(buf,"$Nodes"));

  fgets(buf,BUFSIZ,fp);
  sscanf(buf, "%d", &(mesh->Nnodes));

  //*VX = (dfloat*) calloc(Nnodes, sizeof(dfloat));
  //*VY = (dfloat*) calloc(Nnodes, sizeof(dfloat));
  mesh->VX = (dfloat*) calloc(mesh->Nnodes, sizeof(dfloat));
  mesh->VY = (dfloat*) calloc(mesh->Nnodes, sizeof(dfloat));
  
  for(n=0; n<mesh->Nnodes; ++n){
    fgets(buf, BUFSIZ, fp);
    sscanf(buf, "%*d%f%f", mesh->VX+n, mesh->VY+n);
  }

  do{
    fgets(buf, BUFSIZ, fp);
  }while(!strstr(buf, "$Elements"));

  fgets(buf, BUFSIZ, fp);
  sscanf(buf, "%d", &(mesh->Nelements));

  //(*EToV) = (iint*) calloc(Nelements*Nverts, sizeof(iint));
  mesh->EToV = (iint*) calloc(mesh->Nelements*mesh->Nverts, sizeof(iint));
  
  int Ntriangles = 0;
  for(n=0; n<mesh->Nelements; ++n)
    {
      iint elementType, v1, v2, v3;
      
      fgets(buf, BUFSIZ, fp);
      sscanf(buf, "%*d%d", &elementType);
      if(elementType == 2)   //triangle
	{
	  sscanf(buf, "%*d%*d%*d%*d%*d %d%d%d", &v1, &v2, &v3);
	  mesh->EToV[Ntriangles*mesh->Nverts+0] = v1;
	  mesh->EToV[Ntriangles*mesh->Nverts+1] = v2;
	  mesh->EToV[Ntriangles*mesh->Nverts+2] = v3;
	  ++Ntriangles;
	}
    }

  fclose(fp);

  // *Nnodes_out = mesh->Nnodes;
  //*Ntriangles_out = Ntriangles;
  mesh->Nelements = Ntriangles;
  return mesh;
}

int main(int argc, char **argv){

  mesh2D *mesh;
  mesh = meshReader2D(argv[1]);
  
  
  for(int e=0; e<mesh->Nelements; ++e)
    {
      printf("%d %d %d\n", mesh->EToV[e*mesh->Nverts+0],mesh->EToV[e*mesh->Nverts+1],
	     mesh->EToV[e*mesh->Nverts+2]);
    }
  return 0;
}

/*
int main(int argc, char **argv)
{
  dfloat *VX, *VY;
  iint *EToV;
  int Nnodes, Ntriangles;

  meshReader2D(argv[1],
	       &Nnodes,
	       &VX,
	       &VY,
	       &Ntriangles,
	       &EToV);

  for(int e=0; e<Ntriangles; ++e)
    {
      printf("%d %d %d\n", EToV[e*3+0], EToV[e*3+1], EToV[e*3+2]);
    }
  return 0;
}*/
